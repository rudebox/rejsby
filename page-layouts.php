<?php

/*
 * Template Name: Layouts
 */

get_template_part('parts/header'); the_post(); 

$hide = get_field('show_cta');
$hide_bg = get_field('show_bg');

?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<?php get_template_part('parts/content', 'layouts'); ?>

	<?php if ($hide === false) : ?>
		<?php get_template_part('parts/cta'); ?>
	<?php endif; ?>

  	<?php get_template_part('parts/social'); ?>

 

</main>

<?php get_template_part('parts/footer'); ?>
