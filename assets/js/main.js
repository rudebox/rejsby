jQuery(document).ready(function($) {


  //in viewport check
  var $animation_elements = $('.anim');
  var $window = $(window);

  function check_if_in_view() {
    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);
   
    $.each($animation_elements, function() {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);
   
      //check to see if this current container is within viewport
      if ((element_top_position <= window_bottom_position)) {
        $element.addClass('in-view');
      } else {
        $element.removeClass('in-view');
      }
    });
  }

  $window.on('scroll resize', check_if_in_view);
  $window.trigger('scroll');

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //fancybox
  $('[data-fancybox]').fancybox({ 
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    }
  });

  //responsive YT
  $('#iframeAvailablePlaces').wrap("<div class='embed embed__item embed--16-9'></div>");
  $('iframe[src*="youtube"]').wrap("<div class='embed embed__item embed--16-9'></div>");
  $('#iframePrice').wrap("<div class='embed embed__item embed--fullmonty'></div>");


  //owl slider/carousel
  var owl = $('.slider__track');

  owl.owlCarousel({
      loop: true,
      items: 1,
      autoplay: true,
      dots: true,
      dotsContainer: '#owl-dots', 
      dotsData: true,
      // nav: true,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200
      // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
  });

  $('.owl-dot').click(function() {
        owl.trigger('to.owl.carousel', [$(this).index(), 1000]);
  });


  //owl slider/carousel
  var owlEmployees = $('.slider__track--employees');

  owlEmployees.owlCarousel({
      loop: true,
      items: 1,
      autoplay: false,
      dots: false,
      nav: true,
      autplaySpeed: 11000,
      mouseDrag: true,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"]
  });

  //toggle contact forms - this pretty redundant and stupid
  $('#js-btn-contact').on('click', function() {
    $(this).addClass('is-active');
    $('#js-form-contact').addClass('is-active');
    $('#js-form-application').removeClass('is-active');
    $('#js-btn-application').removeClass('is-active');
    $('#js-form-signup').removeClass('is-active');
    $('#js-btn-signup').removeClass('is-active');
  });

  $('#js-btn-application').on('click', function() {
    $(this).addClass('is-active');
    $('#js-form-contact').removeClass('is-active');
    $('#js-form-signup').removeClass('is-active');
    $('#js-form-application').addClass('is-active');
    $('#js-btn-contact').removeClass('is-active');
    $('#js-btn-signup').removeClass('is-active');
  });

  $('#js-btn-signup').on('click', function() {
    $(this).addClass('is-active');
    $('#js-form-contact').removeClass('is-active');
    $('#js-form-application').removeClass('is-active');
    $('#js-form-signup').addClass('is-active');
    $('#js-btn-application').removeClass('is-active');
    $('#js-btn-contact').removeClass('is-active');
  });

  //accordion
  var $acc = $('.accordion__title'); 

    $acc.each(function() {
      $(this).click(function() {
        $(this).toggleClass('is-active');
        $(this).next().toggleClass('is-visible');
    });
  });

  //rotate on scroll
  $(window).scroll(function() {
    var theta = $(window).scrollTop() / 1000 % Math.PI;
    $('#Flag').css({ transform: 'rotate(' + theta + 'rad)' });
  });


  //lazyload
  $(function() {
        $('.lazy').Lazy({
          threshold: 600
        });
  });


  //trigger employees desc
  var $toggle = $('.employees__trigger');

  $toggle.on('click', function() {
    
    var data = $(this).data('target');
    var $content = $('.employees__content');

    $content.each(function() {

        if ($(this).attr('id') === data ) {
          $(this).toggleClass('is-active');
        } else {
          $(this).removeClass('is-active');
        }
    });

    $toggle.each(function() {

        if ($(this).attr('id') === data ) {
          $(this).toggleClass('is-active');
        } else {
          $(this).removeClass('is-active');
        }
    });
  });


  //YT video bg
  $(function(){
    $('[data-youtube]').youtube_background(); 
  });

});
