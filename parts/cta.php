<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$title = get_field('cta_title', 'options');
$text = get_field('cta_text', 'options');
$link = get_field('cta_link', 'options');
$link_text = get_field('cta_link_text', 'options');

$bg = get_field('cta_bg');

?>

<?php if ($bg === 'blue') : ?>
<section class="cta padding--both bg--blue">
<?php else: ?>
<section class="cta padding--both bg--grey">
<?php endif; ?>
	<div class="wrap hpad">
		<div class="row">
			<div class="cta__item center col-sm-8 col-sm-offset-2">
				<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
				<a class="btn btn--red cta__btn anim fade-up" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?> <i class="fas fa-angle-right"></i></a>
			</div>
		</div>
	</div>
</section>