<footer class="footer bg--blue" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<?php echo file_get_contents('wp-content/themes/rejsby/assets/img/footer_watermark.svg'); ?>
	<div class="wrap hpad">
		<div class="row">
			
			<?php 
				//social links
				$fb = get_field('fb', 'options');
				$ig = get_field('ig', 'options');
				$sc = get_field('sc', 'options');

				if( have_rows('footer_contact', 'options') ) :
			 ?>

				 <div class="col-sm-4 footer__item footer__item--contact">
				 	<?php 	
				 		

				 		while( have_rows('footer_contact' , 'options') ) : the_row();
							$contact_title = get_sub_field('footer_contact_title', 'options');
							$contact_text = get_sub_field('footer_contact_text', 'options'); 
					?>

				 	<h4 class="footer__title"><?php echo esc_html($contact_title); ?></h4>
					<?php echo $contact_text; ?>

					<?php endwhile; ?>

					<a class="footer__social-link footer__social-link--fb" target="_blank" rel="noopener" href="<?php echo esc_url($fb); ?>"><i class="fab fa-facebook-f"></i></a>
					<a class="footer__social-link footer__social-link--ig" target="_blank" rel="noopener" href="<?php echo esc_url($ig); ?>"><i class="fab fa-instagram"></i></a>
					<a class="footer__social-link footer__social-link--sc" target="_blank" rel="noopener" href="<?php echo esc_url($sc); ?>"><i class="fab fa-snapchat-ghost"></i></a>
				 </div>

			<?php endif; ?>
			
			<div class="col-sm-8">
				<div class="row flex flex--wrap footer__container">

					<?php 
						if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
							the_row();
						$title = get_sub_field('column_title');
						$text = get_sub_field('column_text');
					 ?>

					 <div class="col-sm-6 footer__item footer__item--links">
					 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
						<?php echo $text; ?>
					 </div>

					<?php endwhile; endif; ?>

				</div>
			</div>

		</div>
	</div>
</footer>

<div class="footer__copyright bg--blue">
	<div class="wrap hpad flex flex--end">		
		<span>(c) <?php echo date('Y'); ?> Rejsby Europæiske efterskole / All rights reserved</span>
	</div>
</div>

<?php wp_footer(); ?>              

</body>
</html>
