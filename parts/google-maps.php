<?php 
    // Prepare
    $attributes = '';

    // Zoom
    $zoom = get_field('maps_zoom') ?: 12;
    $attributes .= " data-zoom='$zoom'";

    $maps_title = get_field('maps_title');
    $maps_text = get_field('maps_intro');
    $text = get_field('map_text');

    // Maps settings
    if ( $settings = get_field('maps_settings') ) {
        foreach ( $settings as $setting ) {
            $attributes .= " data-$setting='true'";
        }
    }
 ?>

<?php if ($text) : ?>
<section class="google-map google-map--overlay padding--top bg--blue">
<?php else : ?>
<section class="google-map padding--both bg--blue">
<?php endif; ?>

    <?php if ($maps_title) : ?>
        <div class="wrap hpad">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 center google-map__intro">
                    <h2 class="google-map__title"><?php echo esc_html($maps_title); ?></h2>
                    <?php if ($maps_text) : ?>
                        <?php echo $maps_text; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($text) : ?>
    <div class="google-map__wrap">
        <div class="wrap hpad google-map__container">
            <?php if ($maps_title && $maps_text) : ?>
            <div class="google-map__text google-map__text--top">
            <?php else : ?>
            <div class="google-map__text">
            <?php endif; ?>
                <?php echo $text; ?>    
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="google-map__map js-maps" <?php echo $attributes; ?>>

        <?php
            //google map    
            $location = get_field('locations');
            $location['address'];
            $address = get_field('address') ?: $location['address']; 
         ?>
        
        <div class="google-map__marker marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">

        </div>

    </div>
</section>