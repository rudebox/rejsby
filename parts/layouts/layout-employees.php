<?php 
/**
* Description: Lionlab employee repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('employee') ) :
?>

<section class="employees padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap hpad">

		<?php if ($title) : ?>
		<h2 class="employees__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<div class="row flex flex--wrap">
			<div class="slider__track--employees is-slider is-slider--employees">

				<?php 
					while (have_rows('employee') ) : the_row();

						$img = get_sub_field('img');
						$name = get_sub_field('name');
						$position = get_sub_field('position');
						$mail = get_sub_field('mail');
						$text = get_sub_field('text');
						$desc = get_sub_field('desc_text');
	 			 ?>

	 			 <div class="employees__item">
	 			 	<?php if(!empty($img) ) : ?>
	 			 	<div class="employees__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	 			 	</div>
	 			 	<?php endif; ?>
	 			 	<?php if(empty($img) ) : ?>
	 			 	<div class="employees__img--empty">
	 			 		 <i class="fas fa-camera"></i>
	 			 	</div>
	 			 	<?php endif; ?>
	 			 	<div class="flex flex--wrap flex--justify flex--center employees__meta">
	 			 		<div class="employees__wrap">							
	 			 			<p class="employees__position"><?php echo esc_html($position); ?></p>
	 			 			<h5 class="employees__name"><?php echo esc_html($name); ?></h5>
	 			 		</div>

	 			 		<span class="employees__trigger" id="<?php echo esc_attr($name); ?>" data-target="<?php echo esc_attr($name); ?>">
	 			 	 		<i class="fas fa-plus"></i>
	 			 		</span>
 			 		</div>
 			 		<div class="employees__content" id="<?php echo esc_attr($name); ?>">
				 		<?php if ($text) : ?>
		 			 	<p class="employees__text"><?php echo $text; ?></p>
		 			 	<?php endif; ?>
		 			 	<?php if ($desc) : ?>
	 			 		<div class="employees__desc">
	 			 			<?php echo $desc; ?>	 			 						 			
	 			 		</div>
	 			 		<?php endif; ?>	 

	 			 		<?php if ($mail) : ?>
					 		<a class="employees__link" href="mailto:<?php echo esc_html($mail); ?>">	<?php echo esc_html($mail); ?>
					 		</a>
					 	<?php endif; ?>
 			 		</div>
	 			 </div>
	
 			<?php endwhile; ?>
 			</div>
		</div>
	</div>
</section>
<?php endif; ?>