<?php 
    // Prepare
    $attributes = '';

    // Zoom
    $zoom = get_sub_field('maps_zoom') ?: 12;
    $attributes .= " data-zoom='$zoom'";

    // Maps settings
    if ( $settings = get_sub_field('maps_settings') ) {
        foreach ( $settings as $setting ) {
            $attributes .= " data-$setting='true'";
        }
    }

    $margin = get_sub_field('margin');
    $text = get_sub_field('map_text');
 ?>

<?php if ($text) : ?>
<section class="google-map google-map--overlay padding--<?php echo esc_attr($margin); ?>">
<?php else : ?>
<section class="google-map padding--<?php echo esc_attr($margin); ?>">
<?php endif; ?>

    <?php if ($text) : ?>
    <div class="google-map__wrap">
        <div class="wrap hpad google-map__container">
            <div class="google-map__text">
                <?php echo esc_html($text); ?>    
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="google-map__map js-maps" <?php echo $attributes; ?>>

        <?php
            //google map    
            $location = get_sub_field('locations');
            $location['address'];
            $address = get_sub_field('address') ?: $location['address']; 
         ?>
        
        <div class="google-map__marker marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">

        </div>

    </div>
</section>