<?php 
/**
* Description: Lionlab profile courses repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$img = get_sub_field('img');

if (have_rows('linkbox') ) :
?>

<section class="profile-course overlay--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap--fluid">

		<h2 class="profile-course__header center"><?php echo esc_html($title); ?></h2>

		<div class="row flex flex--wrap">
			<div class="slider__track is-slider is-slider--small col-sm-6 col-sm-offset-3">

				<?php while (have_rows('linkbox') ) : the_row(); 
					$title = get_sub_field('title');
					$img = get_sub_field('img');
					$link = get_sub_field('link');
					$tagline = get_sub_field('tagline');
				?>

				<a href="<?php echo esc_url($link); ?>" class="profile-course__item" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
					<h3 class="profile-course__title"><?php echo esc_html($tagline); ?></h3>
					<span class="profile-course__link">Læs mere om <?php echo esc_html($title); ?> her</span>
				</a>
				<?php endwhile; ?>
				
			</div>
		</div>
		
		<div class="is-slider">
			<ul id="owl-dots" class="owl-dots flex flex--wrap flex--valign"> 

			  <?php 
			  	while (have_rows('linkbox') ) : the_row(); 
			  		$title = get_sub_field('title');
			  ?>
			  	<li data-dot="<?php echo esc_html($title); ?>" class="owl-dot"><?php echo esc_html($title); ?></li> 
			  <?php endwhile; ?>

		   	</ul>
	   	</div>

	</div>
</section>
<?php endif; ?>