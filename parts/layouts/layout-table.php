<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('table') ) : ?>

<section class="table bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">
			<div class="col-sm-8 col-sm-offset-2"> 

			<?php if ($title) : ?>
				<h2 class="table__header"><?php echo esc_html($title); ?></h2>
			<?php endif; ?>

			
				<table class="table__table">
					<tbody>
						<?php 
							 while (have_rows('table') ) : the_row(); 
								$time = get_sub_field('time');
								$activity = get_sub_field('acitivity');
								$icon = get_sub_field('icon');
						?>
							
							<tr class="anim fade-up">
								<td class="table__cell table__cell--1">
									<?php echo esc_html($time); ?>
								</td>
								<td class="table__cell table__cell--2">
									<?php echo $icon; ?>
								</td>
								<td class="table__cell table__cell--3">
									<?php echo esc_html($activity); ?>
								</td>
							</tr>

						<?php endwhile; ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</section>
<?php endif; ?>