<?php 
/**
* Description: Lionlab video field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$bg_img = get_sub_field('bg_img');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('header_text');

if (have_rows('video_links') ) :

?>

<?php if ($bg_img) : ?>
<section class="video-references padding--<?php echo esc_attr($margin); ?> overlay--<?php echo esc_attr($bg); ?>" style="background-image: url(<?php echo esc_url($bg_img['url']); ?>);">
<?php else : ?>
<section class="video-references padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
<?php endif; ?>
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="video-references__header center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php if ($text) : ?>
				<div class="video-references__header-text center col-sm-8 col-sm-offset-2">
					<?php echo $text; ?>						
				</div>
			<?php endif; ?>

			<?php 
				while (have_rows('video_links') ) : the_row();

					$link = get_sub_field('video_link');
					$video_title = get_sub_field('video_title');
					$thumbnail = get_sub_field('video_thumbnail');

					//trim url for easier string replacement
					$trim_url = parse_url($link, PHP_URL_PATH);

					//replace and strip string from videolink variable down to video ID for thumbnail use
					$video_id = str_replace('/embed/', '', $trim_url);

 			 ?>	
				
				<?php if ($link) :  ?>
				<div class="col-sm-4 video-references__item anim fade-up">
					<div class="embed embed__item embed--4-3">
						<?php if ($thumbnail) : ?>
						<a class="video-references__link" style="background-image: url(<?php echo esc_url($thumbnail['url']); ?>);" data-fancybox data-type="iframe" data-src="<?php echo esc_url($link); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;">
						<?php else : ?>
						<a class="video-references__link" style="background-image: url(https://i.ytimg.com/vi/<?php echo esc_html($video_id); ?>/maxresdefault.jpg);" data-fancybox data-type="iframe" data-src="<?php echo esc_url($link); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;">
						<?php endif; ?>
							<div class="btn--play video-references__play"><i class="fas fa-play"></i></div>
							<?php if ($video_title) : ?>
						 	<h4 class="video-references__title"><span><?php echo esc_html($video_title); ?></span><div class="blend"></div></h4>
						 	<?php endif; ?>
						</a>
					</div>		
				</div>
				<?php endif; ?>
			
			<?php endwhile; ?>
		</div>
		
		<?php 
			$channel_link = get_sub_field('channel_link');
			if ($channel_link) : 
		?>
			
			<div class="center video-references__channel anim fade-up">
				<a target="_blank" rel="noopener" class="btn btn--red no-ajax" href="<?php echo esc_url($channel_link); ?>">Alle videoer <i class="fas fa-angle-right"></i></a>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php endif; ?>