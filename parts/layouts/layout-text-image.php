<?php
/**
* Description: Lionlab text-image field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//text column
$title = get_sub_field('header');
$position = get_sub_field('text_position');
$text = get_sub_field('text');

//image/video column
$column_img = get_sub_field('column_img');
$column_img_position = get_sub_field('column_img_position');
$type = get_sub_field('type');
$video = get_sub_field('column_video');
$video_text = get_sub_field('column_video_text');
$video_thumbnail = get_sub_field('column_video_thumbnail');

//trim url for easier string replacement
$trim_url = parse_url($video, PHP_URL_PATH);

//replace and strip string from videolink variable down to video ID for thumbnail use
$video_id = str_replace('/embed/', '', $trim_url);


//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
$img = get_sub_field('img');

if ($position === 'right') {
	$row_class = 'flex--reverse';
}

if ($column_img_position === 'bottom') {
	$img_column_class = 'bottom';
	$content_column_class = 'padding--bottom';
}

?>

<?php if ($img) : ?>
<div class="text-image__wrapper" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<section class="text-image padding--<?php echo esc_attr($margin); ?> overlay--<?php echo esc_attr($bg); ?> <?php echo esc_attr($img_column_class); ?>">
<?php else : ?>
<div class="text-image__wrapper">
	<section class="text-image padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>"> 
<?php endif; ?>

		<div class="wrap hpad">
			<div class="row flex flex--wrap <?php echo esc_attr($row_class); ?>">

				<div class="text-image__content col-md-6 <?php echo esc_attr($content_column_class); ?>">
					
					<?php if ($title) : ?>
						<h2 class="text-image__title"><?php echo $title; ?></h2>
					<?php endif; ?>

					<?php echo $text; ?>
				</div>
				
				<?php if ($type === 'img') : ?>
				<div class="text-image__image col-md-6 <?php echo esc_attr($img_column_class); ?>">
					<img class="lazy" data-src="<?php echo esc_url($column_img['url']); ?>" alt="<?php echo $column_img['alt']; ?>">
			    </div>
				<?php endif; ?>

				<?php if ($type === 'video') : ?>
				<div class="text-image__image col-md-6">
					<div class="embed embed__item embed--4-3">
						<?php if (!$video_thumbnail) : ?>
						<a class="video-references__link" style="background-image: url(https://i.ytimg.com/vi/<?php echo esc_html($video_id); ?>/maxresdefault.jpg);" data-fancybox data-type="iframe" data-src="<?php echo esc_url($video); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;"><div class="btn--play video-references__play"><i class="fas fa-play"></i></div>
							<?php if ($video_text) : ?>
							<h4 class="video-references__title"><span><?php echo esc_html($video_text); ?></span><div class="blend"></div></h4>
							<?php endif; ?>
						</a>
						<?php else : ?>
						<a class="video-references__link" style="background-image: url(<?php echo esc_url($video_thumbnail['url']); ?>);" data-fancybox data-type="iframe" data-src="<?php echo esc_url($video); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;"><div class="btn--play video-references__play"><i class="fas fa-play"></i></div>
							<?php if ($video_text) : ?>
							<h4 class="video-references__title"><span><?php echo esc_html($video_text); ?></span><div class="blend"></div></h4>
							<?php endif; ?>
						</a>
						<?php endif; ?>
					</div>	
			    </div>
				<?php endif; ?>

			</div>
		</div>
	</section>
</div>
