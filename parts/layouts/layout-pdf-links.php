<?php 
/**
* Description: Lionlab PDF/Links repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('pdf_link') ) :
?>

<section class="pdf-links bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="pdf-links__header"><?php echo $title; ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('pdf_link') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$type = get_sub_field('type');
				$external_link = get_sub_field('extern_link');
				$internal_link = get_sub_field('intern_link');
				$pdf_file = get_sub_field('pdf_file');
			?>

			<?php if ($type === "pdf") : ?>
			<a target="_blank" rel="noopener" href="<?php echo esc_url($pdf_file); ?>" class="col-sm-4 pdf-links__item anim fade-up">
			<?php elseif ($type === "external_link") : ?>
			<a target="_blank" rel="noopener" href="<?php echo esc_url($external_link); ?>" class="col-sm-4 pdf-links__item anim fade-up">
			<?php else : ?>
			<a href="<?php echo esc_url($internal_link); ?>" class="col-sm-4 pdf-links__item anim fade-up">
			<?php endif; ?>
				<div class="pdf-links__content">
					<h3 class="pdf-links__title"><?php echo esc_html($title); ?></h3>
					<p><?php echo $text; ?></p>
				</div>
				<span class="pdf-links__btn btn">Læs mere <i class="fas fa-angle-right"></i></span>
			</a>

			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>