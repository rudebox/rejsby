<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('profile_course') ) :
?>

<section class="courses bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="link-boxes__header center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">

			<?php while (have_rows('profile_course') ) : the_row(); 
				$title = get_sub_field('title');
				$link = get_sub_field('link');
				$link_info = get_sub_field('link_infobox');
				$img = get_sub_field('img');
				$text = get_sub_field('text');
				$bg = get_sub_field('bg');
				$type = get_sub_field('type');
			?>
			
			<?php if ($type === 'course') : ?>
			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 courses__item anim fade-up" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
				<h3 class="courses__title"><span><?php echo esc_html($title); ?></span><div class="blend"></div></h3>
			</a>
			<?php elseif ($type === 'infobox') : ?> 
			<a href="<?php echo esc_url($link_info); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);" class="col-sm-4 courses__item courses__item--infobox anim fade-up overlay--<?php echo esc_attr($bg); ?>">
				<p><?php echo $text; ?></p>
				<span class="btn btn--red">Udforsk valgfag <i class="fas fa-angle-right"></i></span>
			</a>
			<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif;  ?>