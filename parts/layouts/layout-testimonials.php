<?php 
/**
* Description: Lionlab PDF/Links repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$bg_img = get_sub_field('bg_img');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('center');

if ($center === true) {
	$class = 'center';
}

if (have_rows('testimonial') ) :
?>

<?php if ($bg_img) : ?>
<section class="testimonials overlay--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo esc_url($bg_img['url']); ?>);">
<?php else : ?>
<section class="testimonials bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
<?php endif; ?>
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="testimonials__header <?php echo esc_attr($class); ?>"><?php echo $title; ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('testimonial') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$pdf_file = get_sub_field('pdf_file');
				$img = get_sub_field('img');
			?>

			<a target="_blank" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo esc_url($pdf_file); ?>" class="col-sm-6 testimonials__item anim fade-up">
				<div class="flex flex--wrap testimonials__wrap">
					<div class="testimonials__content col-sm-8">
						<h3 class="testimonials__title"><?php echo esc_html($title); ?></h3>
						<p><?php echo $text; ?></p>
						<span class="testimonials__btn btn">Læs mere <i class="fas fa-angle-right"></i></span>
					</div>
					<div class="testimonials__img col-sm-4" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
					</div>
				</div>
			</a>

			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>