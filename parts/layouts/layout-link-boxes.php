<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$img = get_sub_field('bg_img');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

$intro = get_sub_field('classes_intro');

?>

<?php if ($img) : ?>
<section class="link-boxes bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
<?php else : ?>
<section class="link-boxes bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
<?php endif; ?>
	<div class="wrap hpad">
		<h2 class="link-boxes__header center h1"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			
			<?php if ($intro) : ?>
			<div class="link-boxes__intro col-sm-6 col-sm-offset-3 center">
				<?php echo esc_html($intro); ?>
			</div>
			<?php endif; ?>

			<?php if (have_rows('classes') ) : ?>
				<div class="col-sm-12 link-boxes__classes">

					<div class="flex flex--wrap flex--valign">
						<?php 
							 while (have_rows('classes') ) : the_row(); 
								$class_name = get_sub_field('class_name');
								$class_link = get_sub_field('class_link');
						?>

							<a href="<?php echo esc_url($class_link); ?>" class="link-boxes__class">
								<?php echo esc_html($class_name); ?>
							</a>

						<?php endwhile; ?>
						</div>
				</div>
			<?php endif; ?>

			<?php if (have_rows('linkbox') ) : while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
				$icon = get_sub_field('icon');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-3 link-boxes__item anim fade-up">
				<?php if ($icon) : ?>
				<div class="link-boxes__icon">
					<img src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
				</div>
				<?php endif; ?>
				<h3><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>

				<span class="link-boxes__btn">Læs mere</span>
			</a>
			<?php endwhile; endif;  ?>
		</div>
	</div>
</section>