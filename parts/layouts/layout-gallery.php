<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_sub_field('gallery'); 
$index = get_row_index();


//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('center');

if ($center === true) {
	$class = 'center';
}


if ( $gallery ) : ?> 

	<section class="gallery padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>" itemscope itemtype="ImageGallery">
		<div class="wrap hpad">
			
			<?php if ($title) : ?>
			<h2 class="gallery__title <?php echo esc_attr($class); ?>"><?php echo esc_html($title); ?></h2>
			<?php endif; ?>

			<div class="row gallery__list flex flex--wrap">

				<?php
					// Loop through gallery
					foreach ( $gallery as $image ) : 
				?>


					<div class="gallery__item anim fade-up" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="<?= $image['sizes']['large']; ?>" class="gallery__link no-ajax" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" data-caption="<?= $image['caption']; ?>" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
					
						<img class="gallery__image b-lazy" data-src="<?= $image['sizes']['gallery']; ?>" src="<?= $image['sizes']['gallery']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
						</a>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>