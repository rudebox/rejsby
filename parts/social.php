<?php 
/**
* Description: Lionlab social field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$img = get_field('social_bg', 'options');
$title = get_field('social_title', 'options');
$text = get_field('social_text', 'options');

//social links
$fb = get_field('fb', 'options');
$ig = get_field('ig', 'options');
$yt = get_field('yt', 'options');
?>

<section class="social padding--both overlay--blue-light" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">
			<div class="social__item center col-sm-10 col-sm-offset-1">
				<h2 class="social__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
				
				<div class="social__link-wrap anim fade-up">
					<a class="social__link no-ajax" target="_blank" rel="noopener" href="<?php echo esc_url($fb); ?>"><img class="social__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/Facebook.png" alt="rejsby_facebook"></a>
					<a class="social__link no-ajax" target="_blank" rel="noopener" href="<?php echo esc_url($ig); ?>"><img class="social__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/Instagram.png" alt="rejsby_instagram"></a>
					<a class="social__link no-ajax" target="_blank" rel="noopener" href="<?php echo esc_url($yt); ?>"><img class="social__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/YouTube.png" alt="rejsby_youtube"></a>
				</div>
			</div>
		</div>
	</div>
</section>