<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<?php 
	//contact fields
	$title = get_field('contact_title'); 
	$text = get_field('contact_text'); 
	$messenger_text = get_field('contact_messenger'); 
	$route_link = get_field('route_link'); 
	$route_link_text = get_field('route_link_text'); 
	$bg = get_field('contact_form_bg'); 

	//social fields
	$fb = get_field('fb', 'options');
	$ig = get_field('ig', 'options');
	$lk = get_field('sc', 'options');
	$yt = get_field('yt', 'options');
?>

<main itemscope itemtype="http://schema.org/LocalBusiness">

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="contact contact--form padding--both overlay--blue" style="background-image: url(<?php echo esc_url($bg['url']); ?>);">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-md-6 contact__mail">
					<div class="row">
						<div class="contact__form--controls center col-sm-12">
							<button id="js-btn-contact" class="contact__btn is-active">Kontakt</button>

							<button id="js-btn-application" class="contact__btn">Jobansøgninger</button>
						</div>

						<div class="col-sm-12 contact__forms">
							<div id="js-form-contact" class="contact__form is-active">
							<?php gravity_form( 1, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
							</div>

							<div id="js-form-application" class="contact__form">
							<?php gravity_form( 2, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-5 col-md-offset-1 contact__info">
					<div class="contact__wrap">
						<h2 class="contact__title"><?php echo esc_html($title); ?></h2>
						<?php echo 	$text; ?>
					</div>

					<div class="contact__messenger">
						<h3><?php echo $messenger_text; ?></h3>
						<i class="fas fa-arrow-right"></i>
					</div>
					
				</div>
	
			</div>
		</div>
	</section>	

	<?php get_template_part('parts/employees'); ?>

	<?php get_template_part('parts/google', 'maps'); ?>
	
	<?php if ($route_link) : ?>
	<section class="contact padding--both bg--blue">
		<div class="center">
			<a target="_blank" rel="noopener" class="btn btn--red anim fade-up" href="<?php echo esc_url($route_link); ?>"><?php echo esc_html($route_link_text); ?> <i class="fas fa-angle-right"></i></a>
		</div>
	</section>
	<?php endif; ?>

	<?php get_template_part('parts/social'); ?>

</main>

<?php get_template_part('parts/footer'); ?>