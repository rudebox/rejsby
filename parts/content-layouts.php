<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'slider' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'profile-courses' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'profile-courses' ); ?>

    <?php
    } elseif( get_row_layout() === 'video' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'video' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    } elseif( get_row_layout() === 'courses' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'courses' ); ?>

    <?php
    } elseif( get_row_layout() === 'employees' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'employees' ); ?>

    <?php
    } elseif( get_row_layout() === 'pdf-links' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'pdf-links' ); ?>

    <?php
    } elseif( get_row_layout() === 'gallery' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'gallery' ); ?>

    <?php
    } elseif( get_row_layout() === 'testimonials' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'testimonials' ); ?>

    <?php
    } elseif( get_row_layout() === 'table' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'table' ); ?>

    <?php
    } elseif( get_row_layout() === 'google-map' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'google-map' ); ?>

    <?php
    } elseif( get_row_layout() === 'text-image' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'text-image' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
