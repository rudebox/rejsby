<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage"> 

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PXL3P3V"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div class="toolbar bg--blue">
  <div class="wrap hpad toolbar__container flex flex--center flex--end">
    <form class="toolbar__form" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
      <input class="toolbar__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Indtast søgeord...', 'lionlab'); ?>" name="s" id="s"></input> 
      <button type="submit" class="toolbar__search"><i class="fas fa-search"></i></button>
    </form>
    <?php scratch_cta_nav(); ?>
  </div>
</div>
  
<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--center flex--justify header__container">

    <?php 
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
     ?>


    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/nyt_logo.png" alt="<?php bloginfo('name'); ?>">
      <?php echo $logo_markup; ?> 
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
        <?php scratch_language_nav(); ?>
        <div class="nav--cta">
          <?php scratch_cta_nav(); ?>
        </div>
      </div>
    </nav>

  </div>
</header>