<?php 
// Hero section

$poster = get_field('bg');
?>

<section class="video">
	<div class="video__poster" style="background-image: url(<?php echo $poster['url']; ?>);">
		
		<?php 
		/**
		 * Video
		 **/ 
		$video_mp4 = get_field('mp4');
		$video_ogv = get_field('ogv');
		$video_webm = get_field('webm');
		?>
		
		<?php if ($video_mp4 || $video_ogv || $video_webm) : ?>
		<video class="video__video" playsinline autoplay loop preload="auto" muted volume="0">
			<source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4" codecs="avc1, mp4a">
			<source src="<?php echo esc_url($video_ogv); ?>" type="video/ogg" codecs="theora, vorbis">
			<source src="<?php echo esc_url($video_webm); ?>" type="video/webm" codecs="vp8, vorbis">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>
		<?php endif; ?>
		
		<div class="video__inner wrap hpad clearfix">
			<div class="row flex flex--wrap">

				<?php 
					$title = get_field('title');
					$text = get_field('text');


				 ?>

				<div class="col-sm-6">
					<h2 class="h1 video__title"><?php echo $title; ?></h2>
					<?php echo $text; ?>

					<?php  
						$i=0; 
						if (have_rows('links') ) : while (have_rows('links') ) : the_row(); 

						$link = get_sub_field('link');
						$link_text = get_sub_field('link_text');

						$i++;
					?>	

						<a class="btn video__btn video__btn--<?php echo esc_attr($i); ?>" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?> <i class="fas fa-angle-right"></i></a>

					<?php endwhile; endif; ?>
				</div>

			</div>
		</div> 
	</div>
</section>