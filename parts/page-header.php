<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//bg
	$img = get_field('page_img') ? : $img = get_field('page_img', 'options') ;
	$blog_img = get_field('page_img_blog', 'options');
	$page_video = get_field('page_yt');

	//post img
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

	//content
	$text = get_field('page_text');

	//blog meta
	$author = get_the_author(); 

	$author_id = get_the_author_meta('ID');
    $avatar = get_field('avatar', 'user_'. $author_id );
?>

<?php if (is_home() ) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($blog_img['url']); ?>);">
<?php elseif (is_single() ) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
<?php else : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
<?php endif; ?>
	<?php if ($page_video) : ?>
		<div id="ytbg" data-youtube="<?php echo esc_url($page_video); ?>"></div>
	<?php endif; ?>
	<div class="wrap hpad page__container"></div>
</section>

<section class="page__content bg--grey padding--both center">
	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2">

				<h1 class="page__title"><?php echo esc_html($title); ?></h1>

				<?php if (is_single() ) : ?>
					<div class="single__meta">

		                <img class="single__avatar" src="<?php echo esc_attr($avatar['sizes']['thumbnail']); ?>" alt="<?php echo esc_html($author); ?>">

		                <h6 class="single__name"><?php echo esc_html($author); ?></h6>

	                	<?php if (get_the_author_meta('description') ) : ?>
	                		<em class="single__position"><?php echo (get_the_author_meta('description') ); ?></em>
	                	<?php endif; ?>
						
	                	<span><?php the_time('d/m/Y'); ?></span>
                 	</div>
                <?php endif; ?>
				
				<?php if (!is_search() ) : ?>
					<?php echo $text; ?>
				<?php else: ?>
					<strong>Søgeresultat for:</strong>
  					<?php echo esc_attr(get_search_query()); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>