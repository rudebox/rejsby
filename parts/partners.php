<?php 
/**
* Description: Lionlab partnes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$title = get_field('partners_title', 'options');

if (have_rows('logo', 'options') ) : 
?>

<section class="partners padding--both bg--grey">
	<div class="wrap hpad">
		
		<h2 class="partners__title center"><?php echo esc_html($title); ?></h2>

		<div class="row partners__row">
			<div class="col-sm-8 col-sm-offset-2">

				<div class="row flex flex--center flex--wrap">
					<?php 
						while (have_rows('logo', 'options') ) :
							the_row();
						$img = get_sub_field('img');
						$link = get_sub_field('logo_link');
					 ?>

						 <a target="_blank" rel="noopener" href="<?php echo esc_url($link); ?>" class="col-sm-4 partners__item center anim fade-up">
							<img class="lazy" data-src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
					 	</a>

					<?php endwhile; ?>
				</div>

			</div>
		</div>
	</div>
</section>
<?php endif; ?>