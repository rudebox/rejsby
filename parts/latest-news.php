<?php 
/**
* Description: Retrieve latests post from category news 
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

?>

<?php 
	// Query Arguments
	$args = array(
		'posts_per_page' => 1,
		'order' => 'DESC',
		'category_name' => 'nyheder',
	);

	// The Query
	$news = new WP_Query( $args );

	// The Loop
	if ( $news->have_posts() ) : while ( $news->have_posts() ) : $news->the_post();

?>
	
	<section class="news bg--yellow center">
		<div class="wrap--fluid">
				<a href="<?php echo the_permalink(); ?>" class="news__post">
					<h4 class="news__title"><i class="fas fa-star"></i> <?php the_title(); ?> <i class="fas fa-star"></i></h4>
				</a>
		</div>
	</section>

<?php 
	endwhile; endif;
	/* Restore original Post Data */
	wp_reset_postdata();
 ?>