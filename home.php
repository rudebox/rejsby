<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="archive padding--bottom">

    <div class="wrap hpad">
      <div class="row flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <?php   
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
          ?>

          <a href="<?php the_permalink(); ?>" class="archive__item anim fade-up col-sm-3" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="archive__thumb" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>

            <header class="archive__header">
              <h2 class="archive__title h3" itemprop="headline" title="<?php the_title_attribute(); ?>">
                  <?php the_title(); ?>
              </h2>
                
               <p><?php the_time('d/m/Y'); ?></p> 

              <?php the_excerpt(); ?>
            </header>

            <span class="archive__btn btn">Læs mere <i class="fas fa-angle-right"></i></span>

          </a>

          <?php endwhile; else: ?>

            <p>Ingen indlæg i denne kategori</p>

        <?php endif; ?>

      </div>
    </div>

    <div class="wrap hpad">
      <?php 
        // do pagination
        do_action( 'lionlab_pagination' );
      ?>  
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>