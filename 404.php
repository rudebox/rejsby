<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="padding--bottom">
  	<div class="wrap hpad center">
    	
    	<h4>Vi kunne desværre ikke finde siden du søgte efter</h4>
		<a class="btn btn--red" href="/">Tilbage til forsiden <i class="fas fa-angle-right"></i></a>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>