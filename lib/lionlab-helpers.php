<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 15;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}


	//make gf submit input to button
	function gf_make_submit_input_into_a_button_element($button_input, $form) {

	  //save attribute string to $button_match[1]
	  preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match);

	  //remove value attribute
	  $button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]);

	  return '<button '.$button_atts.'>'.$form['button']['text'].'<i class="fas fa-angle-right"></i></button>';
	}

	add_filter('gform_submit_button', 'gf_make_submit_input_into_a_button_element', 10, 2);


	/**
	 * Extend WordPress search to include custom fields
	 *
	 * https://adambalee.com
	 */

	/**
	 * Join posts and postmeta tables
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
	 */
	function cf_search_join( $join ) {
	    global $wpdb;

	    if ( is_search() ) {    
	        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	    }

	    return $join;
	}
	add_filter('posts_join', 'cf_search_join' );

	/**
	 * Modify the search query with posts_where
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
	 */
	function cf_search_where( $where ) {
	    global $pagenow, $wpdb;

	    if ( is_search() ) {
	        $where = preg_replace(
	            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
	            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	    }

	    return $where;
	}
	add_filter( 'posts_where', 'cf_search_where' );

	/**
	 * Prevent duplicates
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
	 */
	function cf_search_distinct( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
	        return "DISTINCT";
	    }

	    return $where;
	}

	add_filter( 'posts_distinct', 'cf_search_distinct' );


	//Returns pagination element with posibility of using a custom query
	function lionlab_pagination_hook( $query ) {

	  // Fallback to global query
	  if ($query == null) {
	    global $wp_query;
	    $query = $wp_query;
	  }

	  // Need an unlikely integer
	  $big = 999999999;

	  // Arguments
	  $args = array(
	    'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format'             => '?paged=%#%',
	    'total'              => $query->max_num_pages,
	    'current'            => max( 1, get_query_var('paged') ),
	    'show_all'           => false,
	    'end_size'           => 1,
	    'mid_size'           => 1,
	    'prev_next'          => false,
	    'type'               => 'plain',
	  );

	  // Only display if more than 1 page
	  if ( intval($query->max_num_pages) > 1 ) {
	    ob_start() ?>
	    
	    <div class="pagination flex flex--valign">
	      <div class="pagination__nav pagination__previous left"><?php previous_posts_link('<i class="fas fa-angle-left"></i>'); ?></div>
	    
	        <div class="pagination__pages"><?php echo paginate_links($args); ?></div>

	      <div class="pagination__nav pagination__next right"><?php next_posts_link('<i class="fas fa-angle-right"></i>'); ?></div>
	    </div>

	    <?php
	    echo ob_get_clean();
	  }
	}

	add_action( 'lionlab_pagination', 'lionlab_pagination_hook' );

?>