<?php

/*
 * Template Name: Europosten
 */

get_template_part('parts/header'); the_post();

/**
* Description: Lionlab europosten repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

get_template_part('parts/page', 'header'); 

if (have_rows('europost_pub') ) : 
?> 

<main>
	<section class="europost bg--grey padding--bottom">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<?php while (have_rows('europost_pub') ) : the_row(); 
					$pdf = get_sub_field('europost_pdf');
					$desc = get_sub_field('europost_desc');
					$title = get_sub_field('europost_titel');
				?>

				<a data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo esc_url($pdf['url']); ?>" class="col-sm-4 europost__item anim fade-up">
					<?php echo wp_get_attachment_image($pdf['ID'], 'large'); ?>
					<div class="europost__content">
						<h3 class="europost__title"><?php echo esc_html($title); ?></h3>
						<?php echo esc_html($desc); ?>
					</div>
				</a>
				<?php endwhile;  ?>
			</div>
		</div>
	</section>
</main>
<?php endif; ?>

<?php get_template_part('parts/footer'); ?>