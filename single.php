<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="single padding--bottom">

    <div class="wrap hpad">

      <div class="row">

      <article itemscope itemtype="http://schema.org/BlogPosting" class="single__article col-sm-8 col-sm-offset-2">

        <div itemprop="articleBody">
          <?php the_content(); ?>
        </div>
        
        <div class="center padding--top">
          <a class="single__btn btn btn--red" onclick="window.history.go(-1); return false;"><?php _e('Tilbage', 'lionlab'); ?> <i class="fas fa-angle-right"></i></a>
        </div>

      </article>

      </div>

    </div>

  </section>

  <?php get_template_part('parts/cta'); ?>

</main>
  
<?php get_template_part('parts/social'); ?>

<?php get_template_part('parts/footer'); ?>