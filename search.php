<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

  <?php get_template_part('parts/page', 'header');?>


  <section class="search padding--bottom">
    <div class="wrap hpad">
      <div class="row flex flex--wrap">

      <?php if (have_posts()): ?>
        <?php while (have_posts()): the_post(); ?>


            <a href="<?php the_permalink(); ?>" class="archive__item anim fade-up col-sm-3">

            <header class="archive__header">
              <h2 class="archive__title h3" itemprop="headline" title="<?php the_title_attribute(); ?>">
                  <?php the_title(); ?>
              </h2>
            </header>

            <span class="archive__btn btn">Læs mere <i class="fas fa-angle-right"></i></span>

          </a>


          <?php endwhile; else: ?>

            <h4 class="center">Vi kunne ikke finde noget indhold der matchede din søgning for: <span class="red"><?php echo esc_attr(get_search_query()); ?></span></h4>

        <?php endif; ?>

      </div>
    </div>

    <div class="wrap hpad">
      <?php 
        // do pagination
        do_action( 'lionlab_pagination' );
      ?>  
    </div>
  
  </section>

</main>

<?php get_template_part('parts/footer'); ?>