<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/video'); ?>

  <?php get_template_part('parts/latest', 'news'); ?>

  <?php get_template_part('parts/content', 'layouts'); ?>

  <?php get_template_part('parts/cta'); ?>

  <?php get_template_part('parts/social'); ?>

  <?php get_template_part('parts/partners'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
